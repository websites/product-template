/* global q, qa */

"use strict";

var q = document.querySelector.bind(document);
var qa = document.querySelectorAll.bind(document);
NodeList.prototype.forEach = Array.prototype.forEach;
HTMLCollection.prototype.forEach = Array.prototype.forEach;
