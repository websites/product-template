document.addEventListener("DOMContentLoaded", function() {
    if (navigator.doNotTrack != "1" || navigator.doNotTrack != "yes") {
        var _paq = _paq || [];
        _paq.push(["trackPageView"]);
        _paq.push(["enableLinkTracking"]);

        (function() {
            var u = (("https:" == document.location.protocol) ? "https" : "http") + "://" + window.piwik.url + "/";
            _paq.push(["setTrackerUrl", u + "piwik.php"]);
            _paq.push(["setSiteId", window.piwik.id]);
            var d = document,
                g = d.createElement("script"),
                s = d.getElementsByTagName("script")[0];
            g.type = "text/javascript";
            g.defer = true;
            g.async = true;
            g.src = u + "piwik.js";
            s.parentNode.insertBefore(g, s);
        })();
    }
});
