/* global q */

document.addEventListener("DOMContentLoaded", function() {
    if(q("[data-otherslug]")) {
        q(".nav-language a").href = q("[data-otherslug]").dataset.otherslug;
    }
});
