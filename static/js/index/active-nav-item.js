/* global q: true */

document.addEventListener("DOMContentLoaded", function() {
    if(!q(".nav-menu a.active")) {
        q(".nav-menu a:first-of-type").classList.add("active");
    }
});
