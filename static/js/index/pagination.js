/* global q, qa */

"use strict";

var newsItems = qa(".news-item");
var itemsCount = newsItems.length;
var paginateBy = 3;
var prevButton = q(".pagination-prev");
var nextButton = q(".pagination-next");
var currentPage = 1;

var switchPage = function(pageNumber) {
    if(pageNumber < 1) return;
    if(pageNumber === 1) {
        prevButton.disabled = true;
    } else {
        prevButton.disabled = false;
    }
    if(pageNumber >= Math.floor(itemsCount/paginateBy)) {
        nextButton.disabled = true;
    } else {
        nextButton.disabled = false;
    }
    currentPage = pageNumber;
    var lowestShown = ((pageNumber * paginateBy) - paginateBy) + 1;
    var highestShown = (pageNumber * paginateBy);
    newsItems.forEach(function(item, index){
        var order = index + 1;
        if(order >= lowestShown && order <= highestShown) {
            item.style.display = "block";
        } else {
            item.style.display = "none";
        }
    });
};

document.addEventListener("DOMContentLoaded", function() {
    switchPage(currentPage);
});

prevButton.addEventListener("click", function() {
    switchPage(currentPage - 1);
});

nextButton.addEventListener("click", function() {
    switchPage(currentPage + 1);
});
