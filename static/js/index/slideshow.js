/* global q, qa */

document.addEventListener("DOMContentLoaded", function() {
    if (q(".banner-slideshow")) {
        var interval = 4;
        var dots = qa(".slideshow-controls .dot");
        var playButton = q(".slideshow-controls .play");
        var slides = q(".banner-slideshow .slideshow-slides");
        var slideCount = dots.length;
        var currentSlide = 0;
        var sliderInterval;

        var slider = function(slideIndex) {
            var transform = "translateX(-" + slideIndex * 100 / slideCount + "%)"
            slides.style.transform = transform;
            slides.style.webkitTransform = transform;
            dots.forEach(function(dot) {
                dot.classList.remove("active");
            })
            dots[slideIndex].classList.add("active");
        };

        var timer = function(seconds) {
            sliderInterval = setInterval(function() {
                if (currentSlide === slideCount - 1) {
                    currentSlide = 0;
                } else {
                    currentSlide++;
                }
                slider(currentSlide)
            }, seconds * 1000);
        }

        dots.forEach(function(dot) {
            dot.addEventListener("click", function(event) {
                var index = parseInt(event.target.dataset.index);
                clearInterval(sliderInterval);
                currentSlide = index;
                slider(index);
                playButton.classList.remove("hidden");
            });
        });

        playButton.addEventListener("click", function() {
            var slideTo;
            if (currentSlide >= slideCount - 1) {
                slideTo = 0;
            } else {
                slideTo = currentSlide + 1;
            }
            slider(slideTo);
            timer(interval);
            playButton.classList.add("hidden");
        });

        slider(0);
        timer(interval);
    }
});
